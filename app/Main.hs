{-# LANGUAGE LambdaCase,OverloadedStrings,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances,DeriveGeneric,DefaultSignatures #-}

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Clock
import Foreign.Ptr (nullPtr)

import System.Environment
import System.Exit
import Control.Lens.Operators
import System.Socket
import System.Socket.Family.Inet6
import System.Socket.Type.Stream
import System.Socket.Protocol.TCP
import Data.Serialize

import Lib
import Game

import GameConf
import View
import Load
import Input
import Server

gameLoop :: Clock -> Time-> RenderWindow -> GameView -> GameState a ->  IO ()
gameLoop clock dt wnd gV gS = do
  gV <- updateView dt gS gV
  clearRenderWindow wnd black
  drawGame wnd gV
  display wnd
  evt   <- waitEvent wnd
  dt    <- restartClock clock
  case evt of
    Just SFEvtClosed -> return ()
    Just (SFEvtKeyPressed code ctrl alt shift sys)  -> do
      gS <- handleKey code ctrl alt shift sys gS 
      gameLoop clock dt wnd gV gS
    Just (SFEvtMouseButtonPressed button x y)  -> do
      gS <- handleMouse button x y gS
      gameLoop clock dt wnd gV gS
    _ -> do
      gameLoop clock dt wnd gV gS




main  :: IO ()
main = do
  args <- getArgs
  let iniState =  mkInitialState args
  view <- initView (iniState)
  clock <- createClock
  let ctxSettings = Just $ ContextSettings 24 8 0 1 2 [ContextDefault]
  wnd <- createRenderWindow (VideoMode gWIDTH gHEIGHT 32) "Chess" [SFDefaultStyle] ctxSettings
  gameLoop clock timeZero wnd view iniState
  destroy clock
  cleanView view
  destroy wnd


  












-- mai3n = do
--     let ctxSettings = Just $ ContextSettings 24 8 0 1 2 [ContextDefault]
--     wnd <- createRenderWindow (VideoMode 640 480 32) "SFML Haskell Demo" [SFDefaultStyle] ctxSettings
--     let logoPath  = "test.png"
--     let fontPath  = "test.ttf"
--     let musicPath = "test.ogg"
--     tex <- err $ textureFromFile logoPath Nothing
--     spr <- err $ createSprite
--     fnt <- err $ fontFromFile fontPath
--     txt <- err $ createText
--     setTextString txt "Haskell SFML\n  Version 2.0"
--     setTextFont txt fnt
--     setTextCharacterSize txt 20
--     setTextColor txt blue
--     msc <- err $ musicFromFile musicPath
--     play msc
--     setTexture spr tex True
--     loop wnd spr txt
--     destroy msc
--     destroy txt
--     destroy fnt
--     destroy spr
--     destroy tex
--     destroy wnd

 
-- loop :: RenderWindow -> Sprite -> Text -> IO ()
-- loop wnd spr txt = do
--   drawSprite wnd spr Nothing
--   drawText   wnd txt $ Just (renderStates { transform = translation 460 40 })
--   display wnd
--   evt <- waitEvent wnd
--   case evt of
--     Nothing -> return ()
--     Just SFEvtClosed -> return ()
--     _ -> loop wnd spr txt
