{-# LANGUAGE TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module View where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import Control.Lens.Operators
import Control.Monad
import Control.Concurrent.Async
import Linear.V2
import Foreign.Ptr (nullPtr)
import SFML.SFException
import qualified Data.HashMap.Strict as DHS
import Data.HashMap.Strict((!))
import Data.Hashable


import Lib
import Game
import Text
import GameConf
import UI



data GameView = GameView {
  gameV   :: View, 
  texSet  :: DHS.HashMap PSet Texture,
  texBrd  :: (Texture,Sprite),
  pieceV  :: [(Coord,Sprite)],
  fonts   :: DHS.HashMap GameFont Font,
  uiV     :: UIState,
  rotationV :: Float

}

fontPath = "assets/fonts/"
setsPath = "assets/images/pieces/"
uiPath = "assets/images/ui/"

getBoardSprite gV = snd (texBrd gV)

genBoardSprite = do
  spr <- err $ createSprite
  tex <- err $ textureFromFile (uiPath ++ "Board.png") Nothing
  setTexture spr tex True
  return (tex,spr)

genPieceSprite :: DHS.HashMap PSet Texture -> Piece -> IO (Coord,Sprite)
genPieceSprite texts p =
  let offsetY = case (typ p) of 
        Pawn    -> 0
        Bishop  -> 1
        Rook    -> 2
        Queen   -> 3
        King    -> 4 
        Knight  -> 5
      offsetX = case (team p) of
        White -> 0
        Black -> 1
      in do
        spr     <- err $ createSprite
        let tex = texts ! (pSet p) 
        setTexture spr tex True
        setTextureRect spr (IntRect (offsetX*tileSize) (offsetY*tileSize) (tileSize) (tileSize) )
        return (pos p,spr)

createUISprite uiT = do
  tex <- err $ textureFromFile (uiPath ++ (show uiT)++".png") Nothing
  spr <- err $ createSprite
  setTexture spr tex True
  return (uiT,(tex,spr)) 

initView :: GameState a -> IO GameView
initView (InLevel l) = do
  gV <- viewFromRect $ FloatRect 0 0 gWIDTH' gHEIGHT'
  defTex  <- err $ textureFromFile  (setsPath ++ "Dragon.png") Nothing 
  let tS = DHS.fromList [(Dragon,defTex)]
  pV <- mapM (genPieceSprite tS) (l^.pieces)
  
  menuFont <-  err $ fontFromFile $ fontPath ++ "Menu.ttf"

  entexts <-  mapM (createUISprite) [CastleLight,EatLight,ChoiceLight,MoveLight,DestinationLight]

  let uiV' = UIState {
    _textures = DHS.fromList $ entexts,
    _needed = DHS.fromList $[]
  } 
  
  tB <- genBoardSprite 

  return $ GameView {rotationV = 0, texBrd= tB, uiV=uiV',gameV = gV, texSet=tS, pieceV=pV, fonts=DHS.fromList[(MenuFont,menuFont)]}

updateView :: Time -> GameState a -> GameView  -> IO GameView  
updateView dt (InLevel l) gV = do
  pV <- mapM (genPieceSprite (texSet gV)) (l^.pieces)
  uiNeeded <- return $ case l^.turnState of
    ChoiceS _ (Just coord)  -> DHS.fromList $ [(ChoiceLight,[coord])]
    MoveS piece move -> let whereMove (MV place _ _) = place
                            movT mt' = (\(MV _ mt _) -> mt == mt')
                            cstMoves  =   (CastleLight,         concurMap whereMove $ filter (movT CastleM) (legalMoves piece (l^.pieces) piece))
                            uiMoves   =   (DestinationLight,    concurMap whereMove $ filter (movT MoveM) (legalMoves piece (l^.pieces) piece))
                            eatMoves  =   (EatLight,            concurMap whereMove $ filter (movT EatM)  (legalMoves piece (l^.pieces) piece)) in
      DHS.fromList $ (ChoiceLight, [pos piece]) : uiMoves : eatMoves : case move of 
      (Just (MV coord mtype f)) -> (MoveLight,[coord]) : []
      Nothing                   -> [] 
    _ -> DHS.fromList $[]
  let nuiV = (uiV gV) & needed .~ uiNeeded
  -- rotateView (gameV gV) (rotationV gV)
  return gV {pieceV = pV, uiV=nuiV, rotationV = if (currentTeam l == White)then 0 else 180}

cleanView :: GameView -> IO () --destroys all the current items in the view.
cleanView gV = do
  forM_ [(texBrd gV)] (\(t,s)-> destroy t >> destroy s)
  forM_ (pieceV gV) (\(_,s)-> destroy s)
  forM_ (DHS.toList $ texSet gV) (\(_,t) -> destroy t)
  forM_ (DHS.toList $ ((uiV gV)^.textures)) (\(_,(t,s)) -> destroy t >> destroy s)


drawGame ::( SFViewable a, SFRenderTarget a) => a -> GameView -> IO ()
drawGame tg view = do
  setView tg (gameV view)
  drawPiece tg (V2 0 0, getBoardSprite view)
  forM_ (pieceV view)  (drawPiece tg)
  forM_ (DHS.toList $ (uiV view)^.needed) $ \(uiT,places) -> do
    let (_,spr) = (((uiV view)^.textures) ! uiT)
    forM_ places $ \(V2 x y) -> do 
      drawPiece tg (V2 x y,spr)
    
  


drawPiece :: SFRenderTarget a => a -> (Coord,Sprite) -> IO ()
drawPiece tg (V2 x y,spr)  = do
  drawSprite tg spr $ Just $ renderStates  { transform= (translation (offsetX) (offsetY)) * (scaling pieceScale pieceScale)* (translation (tileSize'*x) (tileSize'*y)) }

    