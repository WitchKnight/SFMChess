module Load where
import SFML.Audio
import SFML.Graphics
import SFML.Window
import Lib


loadSetTexture set path = do
  tex <- err $ textureFromFile path Nothing
  return tex


loadPieceSprite path = do
  spr <- err $ createSprite
  tex <- err $ textureFromFile path Nothing

  setTexture spr tex True
  return spr