module GameConf where

import Conf
import Linear.V2
tileSize :: Int
tileSize = 23
tileSize' :: Float
tileSize' = fromIntegral tileSize

pieceScale::Float
pieceScale = 4

gWIDTH::Int
gWIDTH = 1600

gWIDTH'::Float
gWIDTH' = fromIntegral gWIDTH

gHEIGHT ::Int
gHEIGHT  = 900

gHEIGHT'::Float
gHEIGHT' = fromIntegral gHEIGHT

(offsetX,offsetY) = let (V2 bx by) = boardSize in (,)
  (gWIDTH'/2 -  (bx*pieceScale*tileSize')/2 + (pieceScale*tileSize')/2)
  (gHEIGHT'/2 - (by*pieceScale*tileSize')/2 + (pieceScale*tileSize')/2)