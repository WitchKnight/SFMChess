module Text where


import SFML.Audio
import SFML.Graphics
import SFML.Window
import Control.Lens.Operators
import Control.Monad
import Linear.V2
import Foreign.Ptr (nullPtr)
import SFML.SFException
import qualified Data.HashMap.Strict as DHS
import Data.Hashable

import Lib
import Game
  
data GameFont = MenuFont 
              | GameFont deriving (Show,Eq,Ord,Read) 

instance Hashable GameFont where
  hashWithSalt s MenuFont = s + 1
  hashWithSalt s GameFont = s + 2

data GameText = GameText {
  posT  :: V2 Coord,
  fontT :: GameFont,
  sizeT :: Float
}