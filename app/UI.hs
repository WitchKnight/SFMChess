{-# LANGUAGE TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}

module UI where


import SFML.Audio
import SFML.Graphics
import SFML.Window
import Control.Lens
import Control.Monad
import Data.Hashable
import qualified Data.HashMap.Strict as DHS
import Data.HashMap.Strict((!))
import Lib


data UIEType  = ChoiceLight
              | MoveLight
              | DestinationLight 
              | EatLight
              | CastleLight
              deriving (Show,Eq,Read)

instance Hashable UIEType where
  hashWithSalt s ChoiceLight= s + 1
  hashWithSalt s MoveLight = s + 2
  hashWithSalt s DestinationLight = s + 3
  hashWithSalt s EatLight = s + 4
  hashWithSalt s CastleLight = s + 5
                
data UIState = UIState {
  _textures :: DHS.HashMap UIEType (Texture,Sprite),
  _needed   :: DHS.HashMap UIEType [Coord]
}
makeLenses ''UIState