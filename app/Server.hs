{-# LANGUAGE LambdaCase,OverloadedStrings,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances,DeriveGeneric,DefaultSignatures #-}
module Server where

import Control.Exception ( bracket, catch )
import Control.Monad ( forever )
import SFML.Audio
import SFML.Graphics hiding (close, bind)
import SFML.Window hiding (close, bind)
import SFML.System.Clock
import Data.ByteString (pack)
import System.Socket
import System.Socket.Family.Inet6
import System.Socket.Type.Stream
import System.Socket.Protocol.TCP
import System.Environment
import Data.Serialize
import SFML.Audio
import SFML.System.Clock
import Control.Lens hiding (Level)
import Debug.Trace

import Lib
import Game
import GameConf
import View
import Load
import Input

runServer :: IO ()
runServer = bracket
  (socket :: IO (Socket Inet6 Stream TCP))
  (\s -> do
    close s
    putStrLn  "Listening socket closed.")
  (\s -> do
    setSocketOption s (ReuseAddress True)
    setSocketOption s (V6Only False)
    bind s (SocketAddressInet6 inet6Any 8080 0 0)
    listen s 5
    putStrLn "Listen socket ready..."
    (playerW,addrW) <- accept s
    opt1 <- handle White s (playerW,addrW)
    (playerB,addrB) <- accept s
    opt2 <-handle Black s (playerB,addrB)

    let beginGame = case (opt1,opt2) of
                      (Right options1, Right options2) -> do
                        let totopt = options1 ++ options2
                        let optByt = runPut $ put totopt
                        sendGameOptions optByt playerW
                        sendGameOptions optByt playerB
                        let fgS = mkInitialState $ totopt
                        runGameServer s playerW playerB fgS
                      _ -> return ()
    beginGame

    close s
    return ()
    )
    -- let bs = serialiZ gS
    -- let m = (runPut $ put t)

sendGameOptions options  player = do
  send player options msgNoSignal
  return()



handle t s (p,addr)=do
    let m = (runPut $ put t)
    send p m msgNoSignal
    putStrLn $ "Accepted connection from " ++ show addr
    (serOpt,addr) <- receiveFrom p 400 msgNoSignal
    let opt = runGet (get :: Get [String]) serOpt 
    print opt
    return opt





runClient :: IO ()
runClient  = do
  s <- socket :: IO (Socket Inet6 Stream TCP)
  connect s (SocketAddressInet6 inet6Loopback 8080 0 0)
  let finish = print "connection ended" >> System.Socket.close s
  print "successfully connected"
  (str, addr) <- receiveFrom s 30 msgNoSignal
  let team = runGet (get :: Get Team) str in
    case team of
      Left err -> print err >> finish
      Right team' -> do
        print $ "I am "  ++ (show team')
        print "choose your options, enter end when you're done"
        let chOpt "end" = return []
            chOpt other = do 
              print "enter option"
              opt <- getLine
              rest <- chOpt opt
              return $ opt:rest
        options <- chOpt "test"
        print options
        let serOpt = runPut $ put options
        send s serOpt msgNoSignal
        print "?"
        encoptions <- receive s 300 msgNoSignal
        let Right totoptions = runGet (get:: Get [String]) encoptions
        let fgS = mkInitialState totoptions
        clock <- createClock
        view <- initView fgS
        let ctxSettings = Just $ ContextSettings 24 8 0 1 2 [ContextDefault]
        wnd <- createRenderWindow (VideoMode gWIDTH gHEIGHT 32) "Chess" [SFDefaultStyle] ctxSettings
        runGameClient team' s clock timeZero wnd view fgS 
        destroy clock
        cleanView view
        destroy wnd
        finish

runGameClient :: Team
      -> Socket f t1 p
      -> Clock
      -> Time
      -> RenderWindow
      -> GameView
      -> GameState a
      -> IO ()
runGameClient t s clock dt wnd gV gS = do
  gV <- updateView dt gS gV
  clearRenderWindow wnd black
  drawGame wnd gV
  display wnd
  let doTurn = do
        evt   <- waitEvent wnd
        dt    <- restartClock clock
        case evt of
          Just SFEvtClosed -> return ()
          Just (SFEvtKeyPressed code ctrl alt shift sys)  -> do
            gS <- handleKey code ctrl alt shift sys gS 
            runGameClient t s clock dt wnd gV gS
          Just (SFEvtMouseButtonPressed button x y)  -> do
            gS <- handleMouse button x y gS
            runGameClient t s clock dt wnd gV gS
          _ -> do
            runGameClient t s clock dt wnd gV gS
  let waitTurn = do
        -- let waitLoop = do
        --     dt <- restartClock clock
        --     evt <- pollEvent wnd
        --     gV' <- updateView dt gS gV
        --     return gV'
        let pieces' = case gS of  (InLevel l) -> l^.pieces 
                                  _ -> []
        let simpieces = map toSim  $ pieces'
        let msg = runPut $ put simpieces
        send s msg msgNoSignal
        turnInfo <- receive s 800 msgNoSignal
        let Right spieces = deserialiZ turnInfo
        let ngS = case gS of  InLevel l -> trace "updating level" $ InLevel $ unSim spieces $ l & turnState .~ ChoiceS t Nothing
                              _ -> gS
        runGameClient t s clock dt wnd gV ngS
  a <- case gS of
    InLevel l -> if t == (currentTeam l) then
      doTurn  else
      waitTurn
    _ -> return ()
  return ()

runGameServer skt player1 player2 gS = do
  whiteTurnInfo <- receive player1 800 msgNoSignal
  let Right spieces = deserialiZ whiteTurnInfo
  let ngS = case gS of  InLevel l -> InLevel $ unSim spieces l
                        _ -> gS
  let pieces' = case ngS of   (InLevel l) -> l^.pieces 
                              _ -> []
  let simpieces = map toSim  $ pieces'
  let msg = runPut $ put simpieces
  send player2 msg msgNoSignal
  runGameServer skt player2 player1 ngS
  

