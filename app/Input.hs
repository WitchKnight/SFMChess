{-# LANGUAGE TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module Input where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Clock
import Foreign.Ptr (nullPtr)
import Control.Lens.Operators
import Debug.Trace
import Control.Concurrent.Async hiding (cancel)
import Control.Monad
import Linear.V2

import Lib
import Conf
import Game
import GameConf





handleKey :: KeyCode -> Bool -> Bool -> Bool -> Bool -> GameState a -> IO (GameState a) 
handleKey code ctrl alt shift sys gS
  | confirmKey  = traceShow code $ return $ confirm gS
  | cancelKey   = traceShow code $ return $ cancel gS
  | otherwise   = traceShow code $ return gS
  where
    confirmKey = code `elem` [KeyReturn]
    cancelKey = code `elem` [KeyEscape]



handleMouse :: MouseButton -> Int -> Int -> GameState a -> IO (GameState a)
handleMouse button x y gS@(InLevel l)= traceShow (V2 x y ) $ case button of
  MouseLeft   -> do
    okCells <- filterM ((l^.legalVerif) l) emptyBoard
    let transf (V2 x y) = (+) (V2 offsetX offsetY) $ fmap (*(pieceScale)) $ (V2 (tileSize'*x) (tileSize'*y))
    let split pos@(V2 x y) = (,) pos $ transf pos
    let transformCells = map split okCells
    let back = id
   
    let mpos = fmap fromIntegral (V2 x y)
    let selCells = filter (\(_,pos) -> distance pos mpos < pieceScale*tileSize') transformCells
    cell <- return $ case selCells of
      ((c,c'):es)  -> Just c
      _       -> Nothing
    newL <- select cell l
    return $ InLevel $ traceShow cell $ newL
      
  MouseRight  -> return $ confirm gS
  _           -> return $ cancel gS
    