module Lib where



import Prelude hiding (id, (.))
import Control.Arrow
import Control.Category

import System.IO.Unsafe
import Debug.Trace
import Linear.V2

import Control.Concurrent.Async


-----------------------

type Direction = Int
type ActorId = Int
type Coord = V2 Float
justDoIt = unsafePerformIO

distance (V2 x y) (V2 x1 y1) = sqrt $ (x-x1)**2 + (y-y1)**2

concurMap f alist = justDoIt $ mapConcurrently (\x -> return $ f x) alist


fMayb False _ = Nothing
fMayb True x  = Just x

cMayb pred alist= map (\p -> fMayb (pred p) p) alist

applyFuncs [] l = l
applyFuncs (f:fs) l =  applyFuncs fs $ f l 