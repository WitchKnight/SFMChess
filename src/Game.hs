{-# LANGUAGE TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances,DeriveGeneric,DefaultSignatures #-}
module Game where

import qualified Data.ByteString as BS
import Data.Serialize
import GHC.Generics
import Debug.Trace
import Linear.V2
import Linear.V3
import Control.Lens hiding (Level)
import Data.List
import Data.Hashable
import Control.Concurrent.Async hiding (cancel)
import Data.Maybe

import Lib
import Conf

data PType =  Pawn   |
              Bishop |
              Rook   |
              Queen  |
              King   | 
              Knight 
              deriving (Show ,Eq,Read,Generic)

data PSet = Dragon
          | Caesar
          | Arthur
          deriving (Show,Eq,Read,Generic)

instance Hashable PSet where
  hashWithSalt s Dragon = s + 1
  hashWithSalt s Caesar = s + 2
  hashWithSalt s Arthur = s + 3

data Piece  = Piece {
  untouched     :: Bool,
  pName         :: String,
  pSet          :: PSet,
  pos           :: Coord,
  team          :: Team, 
  typ           :: PType,
  legalMoves    :: BoardState -> Piece -> [Move],
  specialPower  :: SpecialP
}

data  SimPiece = SimPiece {
  suntouched     :: Bool,
  spName         :: String,
  spSet          :: PSet,
  spos           :: Coord,
  steam          :: Team, 
  styp           :: PType
}  deriving Generic

toSim piece = SimPiece {
  suntouched = untouched piece,
  spName = pName piece,
  spSet = pSet piece,
  spos = pos piece,
  steam = team piece,
  styp = typ piece
}



instance Serialize Team
instance Serialize PType
instance Serialize PSet
instance Serialize SimPiece


instance Eq Piece where --TODO USE TH TO DO THIS
  p1 == p2 = pos p1 == pos p2 
          && team p1 == team p2
          && typ p1 == typ p2

instance Show Piece where
  show piece =  show (team piece) ++ 
                show (typ piece)  ++ " " ++ (show $ pos piece) ++
                show (pName piece) ++
                "untouched :" ++ (show (untouched piece))

instance Show SimPiece where
  show piece =  show (steam piece) ++ 
                show (styp piece)  ++ " " ++ (show $ spos piece) ++
                show (spName piece) ++
                "untouched :" ++ (show (suntouched piece))


type BoardState =  [Piece]

type LevelF = Level -> Level

data PowerEffect where 
  OnAnyMove   :: LevelF -> PowerEffect
  OnMove      :: MoveType -> LevelF -> PowerEffect
  IfCaptured  :: LevelF -> PowerEffect
  OnCheck     :: PType -> LevelF -> PowerEffect
  IfChecked   :: PType -> LevelF -> PowerEffect
  OnProx      :: Piece -> LevelF -> PowerEffect

getF (OnAnyMove f) = f
getF (OnMove _ f) = f
getF (IfCaptured f) = f
getF (OnCheck _  f) = f
getF (IfChecked _ f) = f
getF (OnProx _  f) = f

type SpecialP = [PowerEffect]

type MoveFunc = (BoardState -> BoardState)
data Team     = White  | Black deriving (Show ,Eq,Read, Generic)
data MoveType = MoveM  | EatM | CastleM | NoM  deriving (Show ,Eq,Read)
data Move     = MV Coord MoveType MoveFunc

instance Show Move where
  show (MV crd mt move) = concat [show crd,  show mt]
  
data TurnState where
  ChoiceS  :: Team -> Maybe Coord -> TurnState -- turn, cursor
  MoveS    :: Piece -> Maybe Move -> TurnState -- piece moving, move
  WinS     :: Team -> TurnState
  DrawS    :: TurnState

data Level = Level {
  _pieces     :: BoardState,
  _turnState  :: TurnState,
  _turnFunc   :: Level -> Level,
  _legalVerif :: Level -> Coord -> IO Bool,
  _possibleVerif :: Level -> Move -> IO Bool,
  _levelStack :: [Level]
}


makeLenses ''Level
zeroLevel = Level {
  _pieces = basePieces White ++ basePieces Black,
  _turnState = ChoiceS White Nothing,
  _turnFunc = turnFuncChoice,
  _legalVerif = legalVerif',
  _possibleVerif = possibleVerif',
  _levelStack = []
}


getT 'W' = White
getT _ = Black

initialState ::  GameState Level
initialState = InLevel zeroLevel

mkInitialState :: [String] -> GameState Level
mkInitialState args =  InLevel $ traceShow (newLevel^.pieces) $ traceShow args $ newLevel
  where
    getFunc arg l = case arg of
      (t:"arthur") -> trace ("replacing " ++ (show (getT t)) ++ " King by Arthur") $ 
        l & pieces %~ alterPiece (getT t) King [(MovesA arthurMoves),(PowerA arthurPower),(NameA "Arthur")]
      (t:"gengis") -> trace ("replacing " ++ (show (getT t)) ++ " King by Gengis") $ 
        l & pieces %~ alterPiece (getT t) King [(MovesA gengisMoves),(PowerA gengisPower),(NameA "Gengis")]
      (t:n:"camelot")-> trace ("replacing " ++ (show n) ++ (show (getT t)) ++ " Rook by Camelot") $
        l & pieces %~ alterPieceOnce (getT t) n Rook [(MovesA camelotMoves),(PowerA camelotPower),(NameA "Camelot")]
      (t:n:"merlin")-> trace ("replacing " ++ (show n) ++ (show (getT t)) ++ " Bishop by Merlin") $
        l & pieces %~ alterPieceOnce (getT t) n Bishop [(MovesA merlinMoves),(PowerA merlinPower),(NameA "Merlin")]
      _             -> l
    allfuncs = map getFunc args
    newLevel = applyFuncs allfuncs zeroLevel
    applyFuncs [] l = l
    applyFuncs (f:fs) l = trace "current pieces" $ traceShow ((f l)^.pieces) $ applyFuncs fs $ f l 

data Alteration = MovesA (BoardState -> Piece -> [Move] )
                | PowerA SpecialP
                | NameA String


alter [] p = p
alter ((MovesA whatToChangeTo):xs) p =  trace "changing moves" $ alter xs $ p {legalMoves    = whatToChangeTo} 
alter ((PowerA whatToChangeTo):xs) p =  trace "changing power" $ alter xs $ p {specialPower  = whatToChangeTo} 
alter ((NameA  whatToChangeTo):xs) p =  trace "changing name"  $ alter xs $ p {pName  = whatToChangeTo} 

alterPieceOnce tm lr ptyp alterations pieces = map f pieces
  where 
    leftP = find (\p -> team p == tm && typ p == ptyp) pieces
    rightP = find (\p -> team p == tm && typ p == ptyp) $ reverse pieces
    f p = if lr=='l' && Just p==leftP then alter alterations p else
          if lr=='r' && Just p==rightP then alter alterations p else
          p
alterPiece tm ptyp alterations pieces = concurMap f pieces
  where
   f p = if team p == tm && typ p == ptyp then alter alterations p else p
   
basePieces :: Team -> [Piece]
basePieces team  = let  r1  = if team==White then 7 else 0
                        r2  = if team==White then 6 else 1 in 
  [
  (bPiece (V2 0 r1) Rook team Dragon noPower),
  (bPiece (V2 1 r1) Knight team Dragon noPower),
  (bPiece (V2 2 r1) Bishop team Dragon noPower),
  (bPiece (V2 3 r1) Queen team Dragon noPower),
  (bPiece (V2 4 r1) King team Dragon noPower),
  (bPiece (V2 5 r1) Bishop team Dragon noPower),
  (bPiece (V2 6 r1) Knight team Dragon noPower),
  (bPiece (V2 7 r1) Rook team Dragon noPower)
  ] ++ [ bPiece (V2 x r2) Pawn team Dragon pawnPower| x <- [0..7]] 

type Selec = ()
data GameState a where
  InSelec :: Selec -> GameState Selec
  InLevel ::  Level -> GameState Level

noPower = []

bPiece :: Coord  -> PType -> Team -> PSet -> (SpecialP ) -> Piece
bPiece pos' typ team' pSet' sPow= piece
  where
    piece  = Piece {pName= "", untouched = True, pSet=pSet',pos=pos', team = team', typ=typ, legalMoves = moves,specialPower = sPow}
    moves = case typ of
      Pawn -> pawnMoves 
      Knight -> knightMoves
      Bishop -> bishopMoves
      Rook -> rookMoves
      Queen -> queenMoves
      King -> kingMoves
      _    -> \x y -> []

nextTeam White = Black
nextTeam Black = White

currentTeam l = case l^.turnState of
  MoveS apiece _ -> team apiece
  ChoiceS team' _ -> team'

confirm :: GameState a -> GameState a
confirm  = \(InLevel l) ->trace "confirming..."  $ InLevel $ confirmg l
cancel :: GameState a -> GameState a
cancel = \(InLevel l) -> trace "canceling..."   $ InLevel $ cancelg l


confirmg l=   nextL 
  where
    nextL= (l^.turnFunc) l
confirmg g = g

cancelg  l= case l^.levelStack of 
  (x:xs) -> x
  _      ->  l

turnFuncChoice = \level -> let  (ChoiceS team which) = level^.turnState
                                piece = case which of
                                  Just coord -> getPosTeam team coord (level^.pieces)
                                  Nothing    -> Nothing in
  case piece of 
    Nothing -> trace "cannot choose no piece... " level
    Just apiece -> trace "going to move" $ level  & turnState .~ MoveS apiece Nothing
                                                  & levelStack %~ (level:)
                                                  & turnFunc.~ turnFuncMove
  
   
turnFuncMove = \level -> let  (MoveS apiece move) = level^.turnState in
  case move of 
    Nothing -> trace "can't move, going back " $ cancelg level
    Just (MV place mType f) -> trace "executing move"  $
      let sPs = cMayb (\p -> case p of 
                      OnMove mt _  -> (mt ==mType)
                      OnAnyMove _    -> True
                      _               -> False) (specialPower apiece)
          sPs' = catMaybes sPs
          allF = [ getF sp | sp <- sPs' ]
          sF = applyFuncs allF
           
          normalNext = level  & turnState .~ ChoiceS (nextTeam (team apiece)) Nothing
                              & levelStack %~ (level:)
                              & turnFunc .~ turnFuncChoice
                              & pieces %~ f in 
        sF normalNext 
        
  


getPos crd bs = find (\p -> pos p == crd ) bs

getPosTeam :: Team -> Coord -> BoardState -> Maybe Piece
getPosTeam tm crd bs = find (\p -> team p == tm && pos p == crd ) bs

getPieceTeam :: Team -> PType -> BoardState -> Maybe Piece
getPieceTeam tm tp bs = find (\p -> team p == tm && typ p == tp ) bs



legalVerif' l coord = case l^.turnState of
  ChoiceS tm _ ->  do
    let tmPieces = filter f $ l^.pieces where  f piece = (team piece  == tm)
    -- let hasMoves p  = case (legalMoves p (l^.pieces) p) of  [] -> False
    --                                                         _  -> True
    -- okPieces <- do
    --   okP <- forConcurrently tmPieces $ \p -> return $  traceShow p $ fMayb (hasMoves p) p
    --   return $ catMaybes okP
    return $ coord `elem` (concurMap pos $ tmPieces)

  MoveS piece _ ->  return $ coord `elem` (concurMap (\(MV crd  _ _) -> crd) $ legalMoves piece (l^.pieces) piece)
 
possibleVerif' l (MV place mt f)= do
    let (MoveS apiece _ ) = l^.turnState 
    dangerMove <- isChecked (team apiece) (f $  l^.pieces)
    return $ not dangerMove

isChecked tm pcs = do
  let (Just king) = getPieceTeam tm King (pcs)
  enemyPieces   <- mapConcurrently (\p -> return  $ fMayb (team p == nextTeam tm) p ) pcs
  enemyMoves    <- mapConcurrently (\p -> return $ legalMoves p pcs p) (catMaybes enemyPieces)
  enemyTargets  <- mapConcurrently (\(MV crd mt _) -> return $ fMayb (mt==EatM) crd) (concat $ enemyMoves)
  let confirmedTargets = catMaybes enemyTargets
  let danger = (pos king) `elem` confirmedTargets
  return $  danger


select maybcoord l =  case maybcoord of
  Nothing -> case l^.turnState of
    MoveS apiece move   -> return $ trace "no valid move selected" $ l & turnState .~ MoveS apiece Nothing
    _                   -> return $ l
  Just (V2 x y) -> case l^.turnState of
    ChoiceS team' coord -> trace ("selected : " ++  (show $ getPosTeam team' (V2 x y ) (l^.pieces))  ) $ return $ l & turnState .~ ChoiceS team' (Just $ V2 x y)
    MoveS apiece move   -> do
          let legalMoves' =(legalMoves apiece (l^.pieces) apiece)
          actualMoves <- forConcurrently legalMoves' $ \m -> do 
            ok' <- (l^.possibleVerif) l m
            return $ fMayb ok' m
          let move'  = find (\(MV crd _ _)  -> crd == V2 x y) (catMaybes actualMoves)
          return $ l & turnState .~ MoveS apiece move'
    _ -> return l 
  


serialiZ:: GameState a -> BS.ByteString
serialiZ (InLevel l) = co
  where
    pieces' = l^.pieces
    spieces = map toSim pieces'
    co = runPut $  (put ::  Putter [SimPiece]) spieces

deserialiZ bs = runGet (get :: Get [SimPiece]) bs

unSim simpieces level = level & pieces %~ (repSim simpieces)




repSim simpieces pieces = map repSim' pieces
  where
    repSim' p = case sp of 
      Nothing -> p
      Just p' ->  traceShow p $ trace "replacing " $ p {pName = spName p',
                    pSet  = spSet p',
                    pos   = spos p',
                    team  = steam p',
                    untouched = suntouched p' }

      where
        sp = find (\sp -> spName sp == pName p  &&
                          spSet sp  == pSet p   &&
                          spos sp   == pos p    &&
                          steam sp  == team p) simpieces

filled board pos'         = pos' `elem` (concurMap pos board)
opteam board piece pos'   = pos' `elem` (concurMap pos (filter (\p -> team p == nextTeam (team piece)) board))
isInBoard crd = crd `elem` emptyBoard

mkMoves  piece [mov,eat] =  concat [map fs mov, map gs eat]
  where
    fs coord = MV coord MoveM $ \pieces -> concurMap (\p -> if p == piece then piece {pos = coord, untouched = False} else p) pieces
    gs coord = MV coord EatM $ \pieces -> concurMap (\p -> if pos p == coord then piece {pos = coord, untouched = False} else p ) (filter (/=piece) pieces)



pawnMoves :: BoardState -> Piece -> [Move]
pawnMoves board piece = mkMoves piece $ concurMap ( filter (isInBoard) ) [legalMoves, legalEat]
  where
    pos' = pos piece
    legalMoves = filter (not. filled board) $ case team piece of
      White -> case pos'^._y of
        6 -> [V2 (pos'^._x) 5, V2 (pos'^._x) 4]
        _ -> [V2 (pos'^._x) (pos'^._y-1)]
      Black -> case pos'^._y of
        1 -> [V2 (pos'^._x) 2, V2 (pos'^._x) 3]
        _ -> [V2 (pos'^._x) (pos'^._y+1)]
    legalEat = filter (opteam board piece) $ filter (filled board) $  case team piece of
      White -> [pos' + V2 1 (-1), pos' + V2 (-1) (-1)]
      Black -> [pos' + V2 1 1, pos' + V2 (-1) 1]   
    

knightMoves ::  BoardState -> Piece -> [Move]
knightMoves board piece =  mkMoves piece $ concurMap ( filter (isInBoard) ) [legalMoves, legalEat]
  where
    pos'@(V2 px py) = pos piece
    legalMoves =  filter (not .filled board) $ 
      buildMoves
    legalEat = filter (opteam board piece) $ filter (filled board) $ 
      buildMoves
    buildMoves = [V2 x y | x <- [px-2..px+2], y<-  [py-2..py+2], distance pos' (V2 x y) == distance (V2 0 0) (V2 1 2)]

bishopMoves :: BoardState -> Piece -> [Move]
bishopMoves board piece =  mkMoves piece $ concurMap ( filter (isInBoard) ) [legalMoves, legalEat]
  where
    pos'@(V2 px py) = pos piece
    legalMoves =  filter (not .filled board) $ 
      buildMoves
    legalEat = filter (opteam board piece) $ filter (filled board) $ 
      buildMoves
    buildMoves = concat [
        buildNE pos',
        buildSE pos',
        buildNW pos',
        buildSW pos'
      ]
    filled' board posi = filled board posi && pos' /= posi
    buildNE (V2 8 _) = []
    buildNE (V2 _ 8) = []
    buildNE (V2 x y)
      | filled' board (V2 x y) = (V2 x y):[]
      | otherwise = (V2 x y): buildNE (V2 (x+1) (y+1))
    buildSE (V2 8 _) = []
    buildSE (V2 _ (-1)) = []
    buildSE (V2 x y)
      | filled' board (V2 x y) = (V2 x y):[]
      | otherwise = (V2 x y): buildSE (V2 (x+1) (y-1))
    buildSW (V2 (-1) _) = []
    buildSW (V2 _ (-1)) = []
    buildSW (V2 x y)
      | filled' board (V2 x y) = (V2 x y):[]
      | otherwise = (V2 x y): buildSW (V2 (x-1) (y-1))
    buildNW (V2 (-1) _) = []
    buildNW (V2 _ 8) = []
    buildNW (V2 x y)
      | filled' board (V2 x y) = (V2 x y):[]
      | otherwise = (V2 x y): buildNW (V2 (x-1) (y+1))


rookMoves :: BoardState -> Piece -> [Move]
rookMoves board piece = castles  ++ normalMoves
  where
    normalMoves = mkMoves piece $ concurMap ( filter (isInBoard) ) [legalMoves, legalEat]
    pos'@(V2 px py) = pos piece
    legalMoves =  filter (not .filled board) $ 
      buildMoves
    legalEat = filter (opteam board piece) $ filter (filled board) $ 
      buildMoves
    buildMoves = concat [
        buildN pos',
        buildS pos',
        buildE pos',
        buildW pos'
      ]
    filled' board posi = filled board posi && pos' /= posi
    buildN (V2 _ 8 ) = []
    buildN (V2 x y)
      | filled' board (V2 x y) =  (V2 x y):[]
      | otherwise =  (V2 x y): buildN (V2 x (y+1))
    buildS (V2 _ (-1) ) = []
    buildS (V2 x y)
      | filled' board (V2 x y) =  (V2 x y):[]
      | otherwise =  (V2 x y): buildS (V2 x (y-1))
    buildW (V2 (-1) _ ) = []
    buildW (V2 x y)
      | filled' board (V2 x y) = (V2 x y):[]
      | otherwise =  (V2 x y): buildW (V2 (x-1) y)
    buildE (V2 8 _ ) = []
    buildE (V2 x y)
      | filled' board (V2 x y) = (V2 x y):[]
      | otherwise =  (V2 x y): buildE (V2 (x+1) y)
    castles :: [Move]
    castles = let r1 = if team piece==White then 7 else 0
                  (Just king) = getPieceTeam (team piece) King board
                  mkBigCastle = mkCastle 3 2
                  mkSmallCastle = mkCastle 5 6
                  mkCastle rx kx = \pieces -> concurMap (\p ->  if p == piece then piece {pos = V2 rx r1 , untouched = False} else
                                                                     if p == king  then king  {pos=V2 kx r1    , untouched = False} else p ) pieces in
      case (pos',untouched piece, untouched king) of 
        ((V2 0 r1),True,True) ->  if filter (filled board) [V2 x' r1 | x' <- [1..3]] == []
                                  then [(MV (pos king) CastleM mkBigCastle)]
                                  else []
        ((V2 7 r1),True,True) ->  if filter (filled board) [V2 x' r1 | x' <- [5,6]] == []
                                  then [(MV (pos king) CastleM mkSmallCastle)]
                                  else []
        _ -> []
    
queenMoves :: BoardState -> Piece -> [Move]
queenMoves bs piece = concat [rM,bM]
  where
    rM = rookMoves bs piece
    bM = bishopMoves bs piece
    

kingMoves :: BoardState -> Piece -> [Move]
kingMoves board piece =  mkMoves piece $ concurMap ( filter (isInBoard) ) [ legalMoves,legalEat]
  where 
    pos'@(V2 px py) = pos piece
    legalMoves = filter (not .filled board) $ 
      buildMoves
    legalEat = filter (opteam board piece) $ filter (filled board) $ 
      buildMoves
    buildMoves = [V2 x y | x <- [px-1..px+1], y <- [py-1..py+1]] 

specialPiece :: Piece -> (SpecialP) -> Piece
specialPiece piece sPow  = piece {specialPower =  sPow} 
----
pawnPower = [promotion]
promotion = OnAnyMove $ f
    where
      f l = result
        where
          team' = nextTeam $ currentTeam l
          r = case team' of 
            White -> 0 
            Black -> 7
          pawn =  find (\p -> typ p == Pawn && team p == team' && (pos p)^._y == r ) $ l^.pieces
          result = case pawn of 
           Just pawn' ->  l & pieces %~ promote pawn'
           Nothing -> l
          
          promote pawn = concurMap (\p -> if p == pawn then pawn {typ = Queen, legalMoves = queenMoves} else p )


-- gengis : When he eats a piece, he can move or eat again.
gengisMoves = kingMoves
gengisPower = [gengisPower']
gengisPower' = OnMove EatM  $ f
  where
    f l = (spF l) & turnFunc .~ newFunc
    spF  = trace "gengis' power activated !" $  \l -> l & turnState .~ ChoiceS (nextTeam $ currentTeam l) Nothing
    newFunc = trace "Gengis is rampaging !" $ realFunc
    realFunc = \level ->             let  (ChoiceS team which) = level^.turnState
                                          piece = case which of
                                            Just coord -> getPosTeam team coord (level^.pieces)
                                            Nothing    -> Nothing in
                                      case piece of 
                                        Nothing -> trace "still choice " level
                                        Just apiece -> case typ apiece of
                                          King-> trace "piece chosen, going to move" (level & turnState .~ MoveS apiece Nothing
                                                                                            & levelStack %~ (level:)
                                                                                            & turnFunc .~ turnFuncMove)
                                          _      -> trace "This is the king's rampage, not anyone else's" level



-- Arthur : When he moves, a knight moves. However if there are no more knights, arthur can not move anymore.
arthurMoves board piece = case getPieceTeam (team piece) Knight board of
  Just _ -> kingMoves board piece
  _      -> []
arthurPower = [arthurPower']
arthurPower' = OnAnyMove $ f
  where
    f l = (spF l) & turnFunc .~ newFunc
    spF l= trace "Arthur's power activated !" $
      l & turnState .~ ChoiceS (nextTeam $ currentTeam l) Nothing
    newFunc = trace "A knight follows his king in battle." $ realFunc
    realFunc =                \level -> let   (ChoiceS team which) = level^.turnState
                                              piece = case which of
                                                Just coord -> getPosTeam team coord (level^.pieces)
                                                Nothing    -> Nothing in
                                        case piece of 
                                          Nothing -> trace "still choice " level
                                          Just apiece -> case typ apiece of
                                            Knight -> trace "piece chosen, going to move" (level  & turnState   .~ MoveS apiece Nothing
                                                                                                  & levelStack  %~ (level:)
                                                                                                  & turnFunc    .~ turnFuncMove)
                                            _      -> trace "You must move a knight after moving Arthur" level 

-- Camelot : when you castle with it, a knight spawns in its previous location.                                            
camelotMoves = rookMoves
camelotPower = [camelotPower']
camelotPower' = OnMove CastleM $ spF
    where
      
      spF l = trace "Camelot's power activated !" $ trace "A noble knight joins the round table."
        l & pieces %~ (\ps -> p:ps)
          where
            (MoveS rook _) = (cancelg l)^.turnState
            p = bPiece (pos rook) Knight (team rook) Dragon noPower


merlinMoves board piece =  (filter (\(MV _ mt _) -> mt == EatM) (bishopMoves board piece) ) ++ merlinMoves
  where
    merlinMoves = mkMerlinMoves$  filter (isInBoard) legalMoves
    mkMerlinMoves places = concurMap f places where
      f coord = MV coord MoveM $ \pieces -> concurMap (\p -> if p == piece then piece {pos = coord, untouched = False} else p) pieces
    pos'@(V2 px py) = pos piece
    legalMoves =  filter (not .filled board) $ 
      buildMoves
    buildMoves = concat [
        buildNE pos',
        buildSE pos',
        buildNW pos',
        buildSW pos'
      ]
    spFilled board pos' team' = (pos',team') `elem` (concurMap (\p -> (pos p, team p)) board)
    filledMerlin board posi = spFilled board posi (team piece)
    filled' board posi = filled board posi && pos' /= posi
    buildNE (V2 8 _) = []
    buildNE (V2 _ 8) = []
    buildNE (V2 x y)
      | filledMerlin board (V2 x y) = buildNE (V2 (x+1) (y+1))
      | filled' board (V2 x y) = (V2 x y):[]
      | otherwise = (V2 x y): buildNE (V2 (x+1) (y+1))
    buildSE (V2 8 _) = []
    buildSE (V2 _ (-1)) = []
    buildSE (V2 x y)
      | filledMerlin board (V2 x y) = buildSE (V2 (x+1) (y-1))
      | filled' board (V2 x y) = (V2 x y):[]
      | otherwise = (V2 x y): buildSE (V2 (x+1) (y-1))
    buildSW (V2 (-1) _) = []
    buildSW (V2 _ (-1)) = []
    buildSW (V2 x y)
      | filledMerlin board (V2 x y) = buildSW (V2 (x-1) (y-1))
      | filled' board (V2 x y) = (V2 x y):[]
      | otherwise = (V2 x y): buildSW (V2 (x-1) (y-1))
    buildNW (V2 (-1) _) = []
    buildNW (V2 _ 8) = []
    buildNW (V2 x y)
      | filledMerlin board (V2 x y) = buildNW (V2 (x-1) (y+1))
      | filled' board (V2 x y) = (V2 x y):[]
      | otherwise = (V2 x y): buildNW (V2 (x-1) (y+1))



merlinPower = noPower