module Conf where


import System.IO.Unsafe
import Debug.Trace
import Linear.V2

import Lib

boardSize = V2 8 8

emptyBoard :: [Coord]
emptyBoard = let V2 sx sy = boardSize in
    [V2 x y | x <- [0..sx-1], y <- [0..sy-1]]

emptyBoard' :: [V2 Int]
emptyBoard' = fmap (fmap truncate) emptyBoard